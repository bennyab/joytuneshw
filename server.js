const express = require('express');
const logger = require('morgan');
const path = require('path');

const greenHouseProvider = require('./routes/GreenHouseProvider');
const port = 3001

const app = express();
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));

//app.use(logger('[:date[clf]] :method :url :status - :response-time ms'));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/providers', greenHouseProvider);

app.listen(port, () => console.log(`app listening on port ${port}!`))

