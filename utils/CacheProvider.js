
'use strict'
var NodeCache  = require('node-cache')

const internal = {};


module.exports = internal.CacheProvider = class  {
    constructor(ttlSeconds) {
      this.cache = new NodeCache({ stdTTL: ttlSeconds, checkperiod: ttlSeconds * 0.2, useClones: false });
    }
    get(key) {
      const value = this.cache.get(key);
      if (value) {
        return value
      }
      return null;
    }

    set(key, data){
        this.cache.set(key, data);
    }
  }