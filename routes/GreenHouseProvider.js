var express = require('express');
var request = require('request');
var router = express.Router();
var JobOffer = require("../data/objects/JobOffer");
var APIResult = require("../data/objects/APIResult");
const CacheProvider = require("../utils/CacheProvider");


let JOY_TUNES_JOBS_GREEN_HOUSE_URL = "https://boards-api.greenhouse.io/v1/boards/joytunes/jobs";

let CACHE_TIME      = 60 * 60 * 1; // 1 hour cache;
let cacheProvider   = new CacheProvider(10);
let ALLJOBS_CACHEKEY = "ALLJOBS_CACHEKEY";

router.get('/GHProvider/getAllJoiTunesJobs', function (req, res, next) {
    
    var data = cacheProvider.get(ALLJOBS_CACHEKEY);
    if (data != null){
        res.send(data);
    }else{
        getAllJobsFromGreenHouse(function(apiResult){
            if (apiResult.isSuccess){
                cacheProvider.set(ALLJOBS_CACHEKEY,apiResult);
            }
            res.send(apiResult);
        })
    }
});

router.get('/GHProvider/getJoyTunesJobDetailed/:jobId',function(req,res,next){
    let jobId = req.params.jobId;
    var cacheKey = "JOBID-" + jobId;
    var data = cacheProvider.get(cacheKey);
    if (data != null){
        res.send(data);
    }else{
        getJobDetails(jobId,function(apiResult){
            if (apiResult.isSuccess){
                cacheProvider.set(cacheKey,apiResult);
            }
            res.send(apiResult); 
        });
    }
});

router.get('/GHProvider/searchJobByTitle/:title',function(req,res,next){
    var data = cacheProvider.get(ALLJOBS_CACHEKEY);
    if (data != null){
        searchJobByTitleResult(res,data,req.params.title);
    }else{
        getAllJobsFromGreenHouse(function(data){
            if (data.isSuccess){
                cacheProvider.set(ALLJOBS_CACHEKEY,data);
            }
            searchJobByTitleResult(res,data,req.params.title);
        })
    }
});

function searchJobByTitleResult(res,data,jobTitle){
    var result = new APIResult();
    if (data == null || data.resultData.length == 0){
        result.isSuccess = false;
        result.errorCode = -1;
        result.errorMessage = "Empty Results For The Wanted Title";
    }else{
        var filteredArray = data.resultData.filter(function(x){
            return x.jobTitle.includes(jobTitle);
        });
        if (filteredArray.length == 0){
            result.isSuccess = false;
            result.errorCode = -1;
            result.errorMessage = "Empty Results For The Wanted Title";
        }else{
            result.resultData = filteredArray;
            result.isSuccess = true;
        }
    }
    res.send(result)
}


// API Integration
function getAllJobsFromGreenHouse(callback){
    request.get({
        url     : JOY_TUNES_JOBS_GREEN_HOUSE_URL,
        json    : true,
    }, (err,res,data) =>{
        var apiResult = new APIResult();
        if (err) {
            apiResult.errorCode = -1;
            apiResult.errorMessage = err;
            console.log('Error:', err);
          } else if (res.statusCode !== 200) {
            apiResult.errorCode = res.statusCode;
            apiResult.errorMessage = res.statusMessage; 
            console.log('Status:', res.statusCode);
          } else {
            var arrOfJobs = data.jobs;
            var parsedArrayResult = new Array();
            arrOfJobs.forEach(element => {
                var offer = new JobOffer();
                offer.parseGreenHouseData(element);
                parsedArrayResult.push(offer);
            });
            apiResult.isSuccess = true;
            apiResult.resultData = parsedArrayResult;
            console.log(data);
          }
          callback(apiResult);
    })
}

function getJobDetails(jobId, callback){
    request.get({
        url     : JOY_TUNES_JOBS_GREEN_HOUSE_URL + "/" + jobId,
        json    : true,
    }, (err,res,data) =>{
        var apiResult = new APIResult();
        if (err) {
            apiResult.errorCode = -1;
            apiResult.errorMessage = err;
            console.log('Error:', err);
          } else if (res.statusCode !== 200) {
            apiResult.errorCode = res.statusCode;
            apiResult.errorMessage = res.statusMessage;  
            console.log('Status:', res.statusCode);
          } else {
              apiResult.isSuccess = true;
              apiResult.resultData = data;
              console.log(data);
          }
          callback(apiResult);
    })
}


module.exports = router;
