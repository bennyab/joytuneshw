'use strict'

var Location = require("./Location");
var Department = require("./Department");
var Office = require("./Office");

const internal = {};

module.exports = internal.JobOffer = class{
    jobTitle;
    jobId;
    location;
    jobOfferUrl;
    lastUpdate;      
    htmlDescription;
    department;
    office;

    constructor (){

    }

    parseGreenHouseData(ghJsonData){
        try {
            this.jobTitle =  ghJsonData.title;
            this.location = new Location();
            this.location.city = ghJsonData.location.name;
            this.jobId = ghJsonData.internal_job_id;
            this.lastUpdate = ghJsonData.updated_at;

        } catch (error) {
            console.log(error);
        }
    }
}